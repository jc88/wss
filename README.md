# wss

## Getting started

## Commands


docker-compose up

docker run --name postgres -p 5432:5432-e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=my_password -d postgres:12-alpine


## Install golang-migrate

brew install golang-migrate


migrate create -ext sql -dir db/migration -seq init_schema


migrate -path db/migration -database "postgresql://postgres:my_password@localhost:5432/simple_bank_db?sslmode=disable" -verbose up
