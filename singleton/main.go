package main

import (
	"fmt"
	"sync"
)

var lock = &sync.Mutex{}

type singleton struct{}

var singletonInstance *singleton

// one way to implement singleton design patten using double lock checking.
func getInstance() *singleton {
	if singletonInstance == nil {
		lock.Lock()
		defer lock.Unlock()
		if singletonInstance == nil {
			fmt.Println("Creating singleton instance now")
			singletonInstance = &singleton{}
		} else {
			fmt.Println("1. Singleton instance already created.")
		}
	} else {
		fmt.Println("2. Singleton instance already created.")
	}
	return singletonInstance
}

func main() {
	for i := 0; i < 100; i++ {
		go getInstance()
	}

	// Stops scanning at a newline
	fmt.Scanln()
}

// // another way to implement singleton design patten using once.Do() will perform the operation only once.
// var once sync.Once
// var singletonInstance *singleton

// func getInstance() *singleton {
// 	if singletonInstance == nil {
// 		once.Do(
// 			func() {
// 				fmt.Println("Creating singleton instance now")
// 				singletonInstance = &singleton{}
// 			})
// 	} else {
// 		fmt.Println("1. Singleton instance already created.")
// 	}
// 	return singletonInstance
// }
