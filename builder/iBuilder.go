//iBuilder.go
package main

type iBuilder interface {
	SetWindowType()
	SetDoorType()
	SetFloorNumber()
	getHouse() house
}

func getBuilder(builderType string) iBuilder {
	if builderType == "normal" {
		return &normalBuilder{}
	}
	if builderType == "igloo" {
		return &iglooBuilder{}
	}
	return nil
}
