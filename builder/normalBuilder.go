//normalBuilder.go
package main

type normalBuilder struct {
	windowType string
	doorType   string
	floor      int
}

func newNormalBuilder() *normalBuilder {
	return &normalBuilder{}
}

func (b *normalBuilder) SetWindowType() {
	b.windowType = "Wooden Window"
}

func (b *normalBuilder) SetDoorType() {
	b.doorType = "Wooden Door"
}

func (b *normalBuilder) SetFloorNumber() {
	b.floor = 2
}

func (b *normalBuilder) getHouse() house {
	return house{
		doorType:   b.doorType,
		windowType: b.windowType,
		floor:      b.floor,
	}
}
