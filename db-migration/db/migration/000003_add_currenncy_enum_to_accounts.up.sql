-- 000003_add_currenncy_enum_to_accounts.up.sql
BEGIN;

CREATE TYPE enum_currency AS ENUM (
	'USD',
	'EUR',
	'JPY',
	'GBP',
	'INR',
	'CAD'
);
ALTER TABLE accounts ALTER COLUMN currency TYPE enum_currency USING currency::enum_currency;

COMMIT;

