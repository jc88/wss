BEGIN;

ALTER TABLE accounts ALTER COLUMN currency TYPE VARCHAR  USING currency::VARCHAR;
DROP TYPE IF EXISTS enum_currency;

COMMIT;
