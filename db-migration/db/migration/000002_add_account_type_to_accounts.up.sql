-- 000002_add_account_type_to_accounts.up.sql
BEGIN;

CREATE TYPE enum_account_type AS ENUM (
	'SAVING',
	'CURRENT',
	'SALARY',
	'FIXED-DIPOSIT',
	'RECURRING-DIPOSIT',
	'NRI'
);
ALTER TABLE accounts ADD COLUMN account_type enum_account_type NOT NULL;

COMMIT;
