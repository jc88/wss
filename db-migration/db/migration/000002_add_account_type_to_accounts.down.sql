-- 000002_add_account_type_to_accounts.down.sql
BEGIN;

ALTER TABLE accounts DROP COLUMN account_type;
DROP TYPE IF EXISTS enum_account_type;

COMMIT;
