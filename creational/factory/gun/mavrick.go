//mavrick.go
package gun

type Maverick struct {
	Gun
}

func NewMaverick() IGun {
	return &Maverick{
		Gun: Gun{
			name:  "Maverick gun",
			power: 5,
		},
	}
}
