// main.go
package main

import (
	"fmt"

	"github.globant.com/design_patterns/creational/factory/gun"
)

func main() {
	ak47, _ := gun.GetGun("ak47")
	maverick, _ := gun.GetGun("maverick")
	printDetails(ak47)
	printDetails(maverick)
}

func printDetails(g gun.IGun) {
	fmt.Printf("Gun: %s, Power: %d\n", g.GetName(), g.GetPower())
}
